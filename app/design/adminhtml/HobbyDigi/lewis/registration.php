<?php



use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::THEME,
    'adminhtml/HobbyDigi/lewis',
    __DIR__
);