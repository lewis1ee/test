<?php


namespace Training4\Vendor2Product\Block;

use Magento\Framework\Registry;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Training4\Vendor\Model\Vendor;
use \Training4\Vendor\Model\ResourceModel\Vendor\Collection;
use \Training4\Vendor\Model\ResourceModel\Vendor\CollectionFactory;

class VendorView extends Template
{
    /**
     * @var null|CollectionFactory
     */
    protected $_vendorCollectionFactory = null;
    /**
     * @var Registry
     */
    private $_registry;

    public function __construct(
        Context $context,
        CollectionFactory $vendorCollectionFactory,
        Registry $registry,
        array $data = []
    )
    {
        $this->_vendorCollectionFactory = $vendorCollectionFactory;
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }

    public function view()
    {
        /** @var Collection $collection */
        $collection = $this->_vendorCollectionFactory->create();
        $collection->getSelect()
            ->join($collection->getTable('training4_vendor2product'),
                'main_table.vendor_id = training4_vendor2product.vendor_id');
        $collection->addFieldToSelect('name')
            ->addFieldToFilter('entity_id', $this->_registry->registry('current_product')->getId());
        return $collection->getItems();
    }


}