<?php


namespace Training4\Vendor2Product\Setup;


use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;


class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;


    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.0.1', '<'))
        {
            $setup->startSetup();
            $collection = $this->collectionFactory->create();
            mt_srand(microtime(true));
            foreach ($collection as $item) {
                $rand = [
                    'vendor_id' => mt_rand(1, 2),
                    'entity_id' => $item->getId(),
                ];
                $setup->getConnection()->insert('training4_vendor2product', $rand);
            }
            $setup->endSetup();
        }
    }
}