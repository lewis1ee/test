<?php


namespace Training4\Vendor2Product\Setup;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '0.0.1', '<')) {
            $tableName = $setup->getTable('training4_vendor2product');
            if ($setup->getConnection()->isTableExists($tableName) != true) {
                $table = $setup->getConnection()->newTable($tableName)
                    ->addColumn(
                        'id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'unsigned' => true,
                            'nullable' => false,
                            'primary' => true,
                        ]
                    )
                    ->addColumn(
                        'vendor_id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => false,
                            'nullable' => false,
                            'auto_increment' => false,
                        ],
                        'Vendor Id'
                    )
                    ->addColumn(
                        'entity_id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => false,
                            'nullable' => false,
                        ],
                        'Entity Id'
                    )
                    ->setComment('Vendor to Product');
                $setup->getConnection()->createTable($table);
            }
        }
        $setup->endSetup();
    }
}