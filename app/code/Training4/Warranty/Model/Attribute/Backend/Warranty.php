<?php


namespace Training4\Warranty\Model\Attribute\Backend;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class Warranty extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
{

    public function beforeSave($object)
    {
        try
        {
            $data = $object->getData('warranty');
            if ($data)
            {
                if ($data == 1) {
                    $object->setData('warranty', $data . ' year');
                } else {
                    $object->setData('warranty', $data . ' years');
                }
            }
            else
            {
                $object->setData('warranty', 1 . ' year');
            }
        }
        catch (\Exception $e)
        {

        }
        parent::beforeSave($object);
    }
}