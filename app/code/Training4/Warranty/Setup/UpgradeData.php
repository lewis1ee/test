<?php


namespace Training4\Warranty\Setup;


use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        if (version_compare($context->getVersion(), '0.1.1', '<'))
        {
            if ($eavSetup->getAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'warranty'))
            {
                $eavSetup->updateAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'warranty',
                    [
                        'type' => 'text',
                        'backend' => 'Training4\Warranty\Model\Attribute\Backend\Warranty',
                        'frontend' => 'Training4\Warranty\Model\Attribute\Frontend\ToBold',
                        'label' => 'warranty',
                        'input' => 'text',
                        'class' => '',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '1',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to' => '',
                        'attribute_set' => 'Gear'
                    ]
                );
            }
            else {
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'warranty',
                    [
                        'type' => 'text',
                        'backend' => 'Training4\Warranty\Model\Attribute\Backend\Warranty',
                        'frontend' => 'Training4\Warranty\Model\Attribute\Frontend\ToBold',
                        'label' => 'warranty',
                        'input' => 'text',
                        'class' => '',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '1',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to' => '',
                        'attribute_set' => 'Gear'
                    ]
                );
            }
        }
    }
}