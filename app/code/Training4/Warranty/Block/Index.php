<?php


namespace Training4\Warranty\Block;
use Magento\Backend\Block\Template\Context;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Eav\Api\AttributeSetRepositoryInterface;
use \Magento\Framework\View\Element\Template;


class Index extends \Magento\Catalog\Block\Product\View\Description
{
    /** @var AttributeSetRepositoryInterface $attributeSet **/
    public $attributeSet;
    protected $_productCollectionFactory;
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    public function __construct(
        Context $context,
        CollectionFactory $productCollectionFactory,
        AttributeSetRepositoryInterface $attributeSet,
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->attributeSet = $attributeSet;
        $this->_coreRegistry = $registry;
        parent::__construct($context, $registry, $data);
    }

    public function getAttributeSetName() {
        $product = $this->getProduct();
        $attributeSetRepository = $this->attributeSet->get((int)$product->getAttributeSetId());
        return $attributeSetRepository->getAttributeSetName();
    }
}