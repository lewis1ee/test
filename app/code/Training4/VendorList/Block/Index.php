<?php


namespace Training4\VendorList\Block;


use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Training4\Vendor\Model\ResourceModel\Vendor\Collection;
use Training4\Vendor\Model\ResourceModel\Vendor\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;

class Index extends Template
{
    /**
     * @var null|CollectionFactory
     */
    protected $_vendorCollectionFactory = null;

    /**
     * Product collection
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected $collection;

    /**
     * @var Registry
     */
    private $_registry;

    public function __construct(
        Context $context,
        CollectionFactory $vendorCollectionFactory,
        Registry $registry,
        ProductCollection $productCollection,
        array $data = []
    )
    {
        $this->_vendorCollectionFactory = $vendorCollectionFactory;
        $this->_registry = $registry;
        $this->collection = $productCollection;
        parent::__construct($context, $data);
    }

    public function getOrder()
    {
        if($this->_request->getParam('sort_by') !== null)
        {
            $order = $this->_request->getParam('sort_by');
        }
        else
        {
            $order = 'ASC';
        }

        return $order;
    }

    public function getVendor()
    {
        /** @var Collection $collection */
        $collection = $this->_vendorCollectionFactory->create();
        $collection->addFieldToSelect('*')->setOrder('vendor_id', $this->getOrder());
        return $collection->getItems();
    }

    public function getProducts()
    {
        $collection = $this->collection->create();

        $vendorId = (int) $this->_request->getParam('vendor_id');


        $collection->addFieldToFilter('entity_id',[
            'in' => new \Zend_Db_Expr(
                "select entity_id from training4_vendor2product where vendor_id = {$vendorId}"
                )
            ])
            ->addAttributeToSort('entity_id', $this->getOrder());


        //var_dump($collection->getSelectSql(true));die();

        return $collection->getItems();
    }


    public function getOrderCollection()
    {
        $collection = $this->_vendorCollectionFactory->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('vendor_id');
        $collection->setOrder('vendor_id')->load();
        return $collection->getItems();
    }

    public function getCategoryProducts($id)
    {
        $collection = $this->collection->create();
        $vendorId = (int) $this->_request->getParam('vendor_id');


        $collection->addFieldToFilter('entity_id',[
            'in' => new \Zend_Db_Expr(
                "select entity_id from training4_vendor2product where vendor_id = {$vendorId}"
            )
        ])
            ->addAttributeToSort('entity_id', $this->getOrder());

        $collection->addCategoriesFilter(['in' => $id]);
        return $collection->getItems();
    }

}