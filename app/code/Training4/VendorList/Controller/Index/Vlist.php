<?php


namespace Training4\VendorList\Controller\Index;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
class Vlist extends Action
{
    private $resultJsonFactory;
    private $orderRepository;
    /**
     * @var LayoutFactory
     */
    private $layoutFactory;
    private $resultPageFactory;
    /**
     * @var CollectionFactory
     */
    private $productRepository;

    public function __construct(JsonFactory $resultJsonFactory,
                                OrderRepositoryInterface $orderRepository,
                                PageFactory $resultPageFactory,
                                LayoutFactory $layoutFactory,
                                CollectionFactory $productRepository,
                                Context $context)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->orderRepository = $orderRepository;
        $this->layoutFactory = $layoutFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->productRepository = $productRepository;
    }

    public function execute()
    {
        $product = $this->productRepository->create();
        $product->addAttributeToSelect('name');
            //->addFieldToFilter('vendor_id', $this->getRequest()->getParam('vendor_id'));
        return $this->resultPageFactory->create();

    }
}