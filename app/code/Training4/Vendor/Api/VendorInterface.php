<?php


namespace Training4\Vendor\Api;


interface VendorInterface
{
    /**#@+
     *
     */
    const VENDOR_ID = 'vendor_id';
    const VENDOR_NAME = 'name';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get title
     *
     * @return string|null
     */
    public function getName();


    /**
     * Set ID
     *
     * @param int $id
     *
     * @return VendorInterface
     */
    public function setId($id);

    /**
     * Set title
     *
     * @param string $name
     *
     * @return VendorInterface
     */
    public function setName($name);
}