<?php


namespace Training4\Vendor\Setup;

use Magento\Catalog\Model\Product;
use Training4\Vendor\Model\Vendor;
use Training4\Vendor\Model\VendorFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    private $vendorFactory;
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    public function __construct(CollectionFactory $collectionFactory,
                                VendorFactory $vendorFactory,
                                EavSetupFactory $eavSetupFactory)
    {
        $this->collectionFactory = $collectionFactory;
        $this->vendorFactory = $vendorFactory;
        $this->eavSetupFactory = $eavSetupFactory;
    }


    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '0.0.1', '<')) {
            /** @var EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                Product::ENTITY,
                'vendor',
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Vendor',
                    'input' => 'text',
                    'class' => '',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );

            $vendors = [
                [
                    'name' => 'Vendor 1'
                ],
                [
                    'name' => 'Vendor 2'
                ]
            ];

            foreach ($vendors as $vendor) {
                $setup->getConnection()->insert('training4_vendor', $vendor);
            }
        }
        $setup->endSetup();
    }
}