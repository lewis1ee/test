<?php


namespace Training4\Vendor\Model;


use Magento\Framework\Model\AbstractModel;
use Training4\Vendor\Api\VendorInterface;

class Vendor extends AbstractModel implements VendorInterface
{
    public function _construct()
    {
        $this->_init("Training4\Vendor\Model\ResourceModel\Vendor");
    }

    /**
     * Retrieve vendor id
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::VENDOR_ID);
    }

    /**
     * Retrieve vendor name
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData(self::VENDOR_NAME);
    }

    /**
     * Set ID
     *
     * @param int $id
     *
     * @return VendorInterface
     */
    public function setId($id)
    {
        return $this->setData(self::VENDOR_ID, $id);
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return VendorInterface
     */
    public function setName($name)
    {
        return $this->setData(self::VENDOR_NAME, $name);
    }
}