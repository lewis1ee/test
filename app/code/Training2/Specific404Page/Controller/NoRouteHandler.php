<?php


namespace Training2\Specific404Page\Controller;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\RequestInterface as Request;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Framework\Controller\ResultFactory;
use mysql_xdevapi\Exception;

class NoRouteHandler implements \Magento\Framework\App\Router\NoRouteHandlerInterface
{
    protected $productRepository;
    protected $categoryRepository;
    protected $result;
    public function __construct(ProductRepository $productRepository,
                                CategoryRepositoryInterface $categoryRepository,
                                \Magento\Framework\Controller\ResultFactory $result)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->result = $result;
    }

    /**
     * @param Request $request
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function process(Request $request)
    {
        $pathInfo = $request->getPathInfo();
        $parts = explode('/', trim($pathInfo, '/'));
        $moduleName = $parts[0] ?? '';
        $actionPath = $parts[1] ?? '';
        $actionName = $parts[2] ?? '';
        if($moduleName == 'catalog' && $actionPath == 'product' && $actionName == 'view')
        {
                $request->setModuleName('notfoundpages')
                    ->setControllerName('noroute')
                    ->setActionName('product');
                return true;
        }
        else if($moduleName == 'catalog' && $actionPath == 'category' && $actionName == 'view')
        {
                $request->setModuleName('notfoundpages')
                    ->setControllerName('noroute')
                    ->setActionName('category');
                return true;
        }
        else {
            $request->setModuleName('notfoundpages')
                ->setControllerName('noroute')
                ->setActionName('other');
            return true;
        }
    }
}