<?php


namespace Training2\OrderController\Controller\Order;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;



class Detail extends Action
{
    protected $_pageFactory;
    protected $json;

    public function __construct(
        Context $context,
        PageFactory $pageFactory
    ){
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        return $this->_pageFactory->create();
    }
}