<?php


namespace Training2\OrderController\Controller\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Sales\Api\OrderRepositoryInterface;

class ReturnJson extends Action
{
    private $resultJsonFactory;
    private $orderRepository;

    public function __construct(JsonFactory $resultJsonFactory,
                                OrderRepositoryInterface $orderRepository,
                                Context $context)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->orderRepository = $orderRepository;
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $orderId = $this->getRequest()->getParam('id');
        try {
            $order = $this->orderRepository->get($orderId);
            $i = 0;
            foreach ($order->getItems() as $item) {
                $items[0][$i] = $item->getSku();
                $items[1][$i] = $item->getItemId();
                $items[2][$i] = $item->getPrice();
                $i++;
            }
            $result->setData([
                'status' => $order->getStatus(),
                'total' => $order->getGrandTotal(),
                'sku' => $items[0],
                'item_id' => $items[1],
                'price' => $items[2],
                'invoiced' => $order->getTotalInvoiced()|0
            ]);
        } catch (\Exception $e)
        {
            $this->messageManager->addErrorMessage('Invalid order id.');
            $this->_redirect('*/*/detail');
        }
        return $result;
    }
}