<?php
declare(strict_types=1);


namespace Training1\FreeGeoIp\Plugin;
use Magento\Customer\Block\CustomerData;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Http\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;

use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Theme\Model\Theme;

class GetCountry
{
    public $remoteAddress;

    private $run = false;

    /**
     * Scope config
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * HTTP Context
     * Customer session is not initialized yet
     *
     * @var Context
     */
    protected $context;

    public function __construct(ScopeConfigInterface $scopeConfig,
                                PageFactory $resultPageFactory,
                                RemoteAddress $remoteAddress,
                                ManagerInterface $messageManager,
                                Context $context)
    {
        $this->scopeConfig = $scopeConfig;
        $this->resultPageFactory = $resultPageFactory;
        $this->remoteAddress = $remoteAddress;
        $this->_messageManager = $messageManager;
        $this->context = $context;
    }


    /**
     * @param \Magento\Customer\Block\CustomerData $subject
     * @param string $route
     * @return array
     */
    public function beforeGetCustomerDataUrl(\Magento\Customer\Block\CustomerData $subject, $route)
    {
        // TODO: Implement plugin method.
        /*$ip = $this->remoteAddress->getRemoteAddress();
        $access_key = '1bda222301fd81640455091d2cc0d82d';
        $ch = curl_init('http://api.ipstack.com/' . $ip . '?access_key=' . $access_key . '');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $json = curl_exec($ch);
        curl_close($ch);

        $api_result = json_decode($json, true);

        $this->_messageManager->addNoticeMessage('Your ip: '. $ip . ' and location: ' . $api_result['location']['capital']);*/
        return [$route];
    }
}