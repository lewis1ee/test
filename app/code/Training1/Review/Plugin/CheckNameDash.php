<?php


namespace Training1\Review\Plugin;

use Magento\Framework\Exception\LocalizedException;
use Magento\Review\Block\Form;
use Magento\Review\Controller\Product\Post;
use Magento\Review\Model\Review;

class CheckNameDash
{
    /**
     * @param \Magento\Review\Model\Review $subject
     */
    public function aroundValidate(\Magento\Review\Model\Review $subject, $proceed)
    {
        $return = $proceed();
        $errors = is_array($return) ? $return : [];
        if(strpos($subject->getData('nickname'), '-') !== false)
        {
            $errors[] = __('Sorry, name should not contain -.');
        }

        if (empty($errors)) {
            return true;
        }
        return $errors;
    }

}