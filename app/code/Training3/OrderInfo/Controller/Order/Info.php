<?php


namespace Training3\OrderInfo\Controller\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\Page;

class Info extends Action
{
    private $resultJsonFactory;
    private $orderRepository;
    /**
     * @var LayoutFactory
     */
    private $layoutFactory;
    private $resultPageFactory;

    public function __construct(JsonFactory $resultJsonFactory,
                                OrderRepositoryInterface $orderRepository,
                                PageFactory $resultPageFactory,
                                LayoutFactory $layoutFactory,
                                Context $context)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->orderRepository = $orderRepository;
        $this->layoutFactory = $layoutFactory;
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $orderId = $this->getRequest()->getParam('id');
        $order = $this->orderRepository->get($orderId);
        $items = [];

        foreach ($order->getItems() as $item) {
            $items[] = [
                'sku' => $item->getSku(),
                'item_id' => $item->getItemId(),
                'price' => $item->getPrice(),
                ];

        }
        if((int)$this->getRequest()->getParam('json', 0) === 1)
        {
            try {
                $result->setData([
                    'status' => $order->getStatus(),
                    'total' => $order->getGrandTotal(),
                    'items' => $items,
                    'invoiced' => $order->getTotalInvoiced() | 0
                ]);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage('Invalid order id.');
                $this->_redirect('*/*/index');
            }
            return $result;
        }
        else
        {
            $result = $this->resultPageFactory->create();
            $result->getConfig()->getTitle()->prepend(__('Order Info'));
            /** @var \Training3\OrderInfo\Block\Index $block */
            $block = $result->getLayout()->getBlock('orderinfo.order.info');
            $block->setData('status', $order->getStatus());
            $block->setData('Total', $order->getGrandTotal());
            $block->setData('Invoiced', $order->getTotalInvoiced()|0);
            return $result;
        }
    }
}