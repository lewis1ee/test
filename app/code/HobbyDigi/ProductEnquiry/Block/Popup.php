<?php


namespace HobbyDigi\ProductEnquiry\Block;

use Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\View\Element\Template;

class Popup extends Template
{
    public function __construct(Context $context,
                                array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function getFormAction()
    {
        // companymodule is given in routes.xml
        // controller_name is folder name inside controller folder
        // action is php file name inside above controller_name folder

        return '/hobbydigi/Index/Enquire';
        // here controller_name is index, action is booking
    }
}